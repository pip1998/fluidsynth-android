APP_STL := gnustl_static
APP_LDFLAGS := -latomic


APP_CPPFLAGS += -fexceptions -frtti -DCC_ENABLE_CHIPMUNK_INTEGRATION=1 -std=c++11 -fsigned-char
APP_ABI := armeabi armeabi-v7a 
APP_OPTIM := release
APP_STL := gnustl_shared
#NDK_TOOLCHAIN_VERSION := 4.7
APP_MODULES := fluidsynth-android
APP_PLATFORM := android-9
