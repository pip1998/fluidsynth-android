/* FluidSynth - A Software Synthesizer
 *
 * Copyright (C) 2003  Peter Hanappe and others.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License
 * as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307, USA
 */

/* fluid_aufile.c
 *
 * Audio driver, outputs the audio to a file (non real-time)
 *
 */

#include "fluid_adriver.h"
#include "fluid_settings.h"
#include "fluid_sys.h"
#include <stdio.h>

#include <assert.h>
// for native audio
#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>
/** fluid_file_audio_driver_t
 *
 * This structure should not be accessed directly. Use audio port
 * functions instead.
 */
typedef struct {
	fluid_audio_driver_t driver;
	fluid_audio_func_t callback;
	void* data;
	int period_size;
	double sample_rate;
	FILE* file;
	fluid_timer_t* timer;
	float* left;
	float* right;
	short* buf;
	int buf_size;
	unsigned int samples;
} fluid_file_audio_driver_t;

// engine interfaces
static SLObjectItf engineObject = NULL;
static SLEngineItf engineEngine;

// output mix interfaces
static SLObjectItf outputMixObject = NULL;
static SLEnvironmentalReverbItf outputMixEnvironmentalReverb = NULL;

// buffer queue player interfaces
static SLObjectItf bqPlayerObject = NULL;
static SLPlayItf bqPlayerPlay;
static SLAndroidSimpleBufferQueueItf bqPlayerBufferQueue;
static SLEffectSendItf bqPlayerEffectSend;
static SLMuteSoloItf bqPlayerMuteSolo;
static SLVolumeItf bqPlayerVolume;

// aux effect on the output mix, used by the buffer queue player
static const SLEnvironmentalReverbSettings reverbSettings =
    SL_I3DL2_ENVIRONMENT_PRESET_STONECORRIDOR;

// URI player interfaces
static SLObjectItf uriPlayerObject = NULL;
static SLPlayItf uriPlayerPlay;
static SLSeekItf uriPlayerSeek;
static SLMuteSoloItf uriPlayerMuteSolo;
static SLVolumeItf uriPlayerVolume;

// file descriptor player interfaces
static SLObjectItf fdPlayerObject = NULL;
static SLPlayItf fdPlayerPlay;
static SLSeekItf fdPlayerSeek;
static SLMuteSoloItf fdPlayerMuteSolo;
static SLVolumeItf fdPlayerVolume;


fluid_audio_driver_t* new_fluid_file_audio_driver(fluid_settings_t* settings,
						  fluid_synth_t* synth);

int delete_fluid_file_audio_driver(fluid_audio_driver_t* p);
void fluid_file_audio_driver_settings(fluid_settings_t* settings);
void bqPlayerCallback(SLAndroidSimpleBufferQueueItf bq, void *context);
void Java_com_example_nativeaudio_NativeAudio_createEngine();
void Java_com_example_nativeaudio_NativeAudio_createBufferQueueAudioPlayer();
static int fluid_file_audio_run_s16(void* d, unsigned int msec);

/**************************************************************
 *
 *        'file' audio driver
 *
 */

void fluid_file_audio_driver_settings(fluid_settings_t* settings)
{
	Java_com_example_nativeaudio_NativeAudio_createEngine();
	Java_com_example_nativeaudio_NativeAudio_createBufferQueueAudioPlayer();
	fluid_settings_register_str(settings, "audio.file.name", "/data/data/com.meet.pianolearn/files/fluidsynth.raw", 0, NULL, NULL);
}


fluid_audio_driver_t*
new_fluid_file_audio_driver(fluid_settings_t* settings,
			    fluid_synth_t* synth)
{
	fluid_file_audio_driver_t* dev;
	int err;
	char* filename;
	int msec;

	dev = FLUID_NEW(fluid_file_audio_driver_t);
	FLUID_LOG(FLUID_ERR, "pip init");
	if (dev == NULL) {
		FLUID_LOG(FLUID_ERR, "Out of memory");
		return NULL;
	}
	FLUID_MEMSET(dev, 0, sizeof(fluid_file_audio_driver_t));

	fluid_settings_getint(settings, "audio.period-size", &dev->period_size);
	fluid_settings_getnum(settings, "synth.sample-rate", &dev->sample_rate);

	dev->data = synth;
	dev->callback = (fluid_audio_func_t) fluid_synth_process;
	dev->samples = 0;
	dev->left = FLUID_ARRAY(float, dev->period_size);
	dev->right = FLUID_ARRAY(float, dev->period_size);
	dev->buf = FLUID_ARRAY(short, 2 * dev->period_size);
	dev->buf_size = 2 * dev->period_size * sizeof(short);

	if (fluid_settings_getstr(settings, "audio.file.name", &filename) == 0) {
		FLUID_LOG(FLUID_ERR, "No file name specified");
		goto error_recovery;
	}

	dev->file = fopen(filename, "wb");
	if (dev->file == NULL) {
		FLUID_LOG(FLUID_ERR, "Failed to open the file '%s'", filename);
		goto error_recovery;
	}

	msec = (int) (0.5 + dev->period_size / dev->sample_rate * 1000.0);
	dev->timer = new_fluid_timer(msec, fluid_file_audio_run_s16, (void*) dev, 1, 0);
	if (dev->timer == NULL) {
		FLUID_LOG(FLUID_PANIC, "Couldn't create the audio thread.");
		goto error_recovery;
	}

	return (fluid_audio_driver_t*) dev;

 error_recovery:
	delete_fluid_file_audio_driver((fluid_audio_driver_t*) dev);
	return NULL;
}

int delete_fluid_file_audio_driver(fluid_audio_driver_t* p)
{
	fluid_file_audio_driver_t* dev = (fluid_file_audio_driver_t*) p;

	if (dev == NULL) {
		return FLUID_OK;
	}

	if (dev->timer != NULL) {
		delete_fluid_timer(dev->timer);
	}

	if (dev->file != NULL) {
		fclose(dev->file);
	}

	if (dev->left != NULL) {
		FLUID_FREE(dev->left);
	}

	if (dev->right != NULL) {
		FLUID_FREE(dev->right);
	}

	if (dev->buf != NULL) {
		FLUID_FREE(dev->buf);
	}

	FLUID_FREE(dev);
	return FLUID_OK;
}

static int fluid_file_audio_run_s16(void* d, unsigned int clock_time)
{
	fluid_file_audio_driver_t* dev = (fluid_file_audio_driver_t*) d;
	int n, offset;
	unsigned int sample_time;

	sample_time = (unsigned int) (dev->samples / dev->sample_rate * 1000.0);
	if (sample_time > clock_time) {
		return 1;
	}

	fluid_synth_write_s16(dev->data, dev->period_size, dev->buf, 0, 2, dev->buf, 1, 2);

	for (offset = 0; offset < dev->buf_size; offset += n) {

		n = fwrite((char*) dev->buf + offset, 1, dev->buf_size - offset, dev->file);
		// SLresult result;
  //       result = (*bqPlayerBufferQueue)->Enqueue(bqPlayerBufferQueue, (char*) dev->buf + offset, dev->buf_size - offset);
        // if (SL_RESULT_SUCCESS != result) {
        // FLUID_LOG(FLUID_ERR, "Audio output failed : %d %d", result, sizeof(dev->buf));

		if (n < 0) {
			FLUID_LOG(FLUID_ERR, "Audio output file write error: %s",
				  strerror (errno));
			return 0;
		}
		FLUID_LOG(FLUID_ERR, "Audio output file writeed: %s", (char*) dev->buf + offset);
	}

	SLresult result;
 	result = (*bqPlayerBufferQueue)->Enqueue(bqPlayerBufferQueue, (char*) dev->buf, dev->buf_size);
	dev->samples += dev->period_size;

	return 1;
}

// this callback handler is called every time a buffer finishes playing
void bqPlayerCallback(SLAndroidSimpleBufferQueueItf bq, void *context)
{
    
}

// create the engine and output mix objects
void Java_com_example_nativeaudio_NativeAudio_createEngine()
{
    SLresult result;

    // create engine
    result = slCreateEngine(&engineObject, 0, NULL, 0, NULL, NULL);
    FLUID_LOG(0, "create %d", SL_RESULT_SUCCESS == result);
    (void)result;

    // realize the engine
    result = (*engineObject)->Realize(engineObject, SL_BOOLEAN_FALSE);
    FLUID_LOG(0, "Realize %d", SL_RESULT_SUCCESS == result);
    (void)result;

    // get the engine interface, which is needed in order to create other objects
    result = (*engineObject)->GetInterface(engineObject, SL_IID_ENGINE, &engineEngine);
    FLUID_LOG(0, "GetInterface %d", SL_RESULT_SUCCESS == result);
    (void)result;

    // create output mix, with environmental reverb specified as a non-required interface
    const SLInterfaceID ids[1] = {SL_IID_ENVIRONMENTALREVERB};
    const SLboolean req[1] = {SL_BOOLEAN_FALSE};
    result = (*engineEngine)->CreateOutputMix(engineEngine, &outputMixObject, 1, ids, req);
    FLUID_LOG(0, "CreateOutputMix %d", SL_RESULT_SUCCESS == result);
    (void)result;

    // realize the output mix
    result = (*outputMixObject)->Realize(outputMixObject, SL_BOOLEAN_FALSE);
    FLUID_LOG(0, "Realize %d",SL_RESULT_SUCCESS == result);
    (void)result;

    // get the environmental reverb interface
    // this could fail if the environmental reverb effect is not available,
    // either because the feature is not present, excessive CPU load, or
    // the required MODIFY_AUDIO_SETTINGS permission was not requested and granted
    result = (*outputMixObject)->GetInterface(outputMixObject, SL_IID_ENVIRONMENTALREVERB,
            &outputMixEnvironmentalReverb);
    if (SL_RESULT_SUCCESS == result) {
        result = (*outputMixEnvironmentalReverb)->SetEnvironmentalReverbProperties(
                outputMixEnvironmentalReverb, &reverbSettings);
        (void)result;
    }
    // ignore unsuccessful result codes for environmental reverb, as it is optional for this example

}


// create buffer queue audio player
void Java_com_example_nativeaudio_NativeAudio_createBufferQueueAudioPlayer()
{
    SLresult result;

    // configure audio source
    SLDataLocator_AndroidSimpleBufferQueue loc_bufq = {SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE, 2};
    SLDataFormat_PCM format_pcm = {SL_DATAFORMAT_PCM, 1, SL_SAMPLINGRATE_44_1,
        SL_PCMSAMPLEFORMAT_FIXED_16, SL_PCMSAMPLEFORMAT_FIXED_16,
        SL_SPEAKER_FRONT_CENTER, SL_BYTEORDER_LITTLEENDIAN};
    SLDataSource audioSrc = {&loc_bufq, &format_pcm};

    // configure audio sink
    SLDataLocator_OutputMix loc_outmix = {SL_DATALOCATOR_OUTPUTMIX, outputMixObject};
    SLDataSink audioSnk = {&loc_outmix, NULL};

    // create audio player
    const SLInterfaceID ids[3] = {SL_IID_BUFFERQUEUE, SL_IID_EFFECTSEND,
            /*SL_IID_MUTESOLO,*/ SL_IID_VOLUME};
    const SLboolean req[3] = {SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE,
            /*SL_BOOLEAN_TRUE,*/ SL_BOOLEAN_TRUE};
    result = (*engineEngine)->CreateAudioPlayer(engineEngine, &bqPlayerObject, &audioSrc, &audioSnk,
            3, ids, req);
    FLUID_LOG(0, "CreateAudioPlayer %d", SL_RESULT_SUCCESS == result);
    (void)result;

    // realize the player
    result = (*bqPlayerObject)->Realize(bqPlayerObject, SL_BOOLEAN_FALSE);
    FLUID_LOG(0, "Realize %d", SL_RESULT_SUCCESS == result);
    (void)result;

    // get the play interface
    result = (*bqPlayerObject)->GetInterface(bqPlayerObject, SL_IID_PLAY, &bqPlayerPlay);
    FLUID_LOG(0, "GetInterface %d", SL_RESULT_SUCCESS == result);
    (void)result;

    // get the buffer queue interface
    result = (*bqPlayerObject)->GetInterface(bqPlayerObject, SL_IID_BUFFERQUEUE,
            &bqPlayerBufferQueue);
    FLUID_LOG(0, "GetInterface %d", SL_RESULT_SUCCESS == result);
    (void)result;

    // register callback on the buffer queue
    result = (*bqPlayerBufferQueue)->RegisterCallback(bqPlayerBufferQueue, bqPlayerCallback, NULL);
    FLUID_LOG(0, "RegisterCallback %d", SL_RESULT_SUCCESS == result);
    (void)result;

    // get the effect send interface
    result = (*bqPlayerObject)->GetInterface(bqPlayerObject, SL_IID_EFFECTSEND,
            &bqPlayerEffectSend);
    FLUID_LOG(0, "GetInterface %d", SL_RESULT_SUCCESS == result);
    (void)result;

#if 0   // mute/solo is not supported for sources that are known to be mono, as this is
    // get the mute/solo interface
    result = (*bqPlayerObject)->GetInterface(bqPlayerObject, SL_IID_MUTESOLO, &bqPlayerMuteSolo);
    assert(SL_RESULT_SUCCESS == result);
    (void)result;
#endif

    // get the volume interface
    result = (*bqPlayerObject)->GetInterface(bqPlayerObject, SL_IID_VOLUME, &bqPlayerVolume);
    FLUID_LOG(0, "GetInterface %d", SL_RESULT_SUCCESS == result);
    (void)result;

    // set the player's state to playing
    result = (*bqPlayerPlay)->SetPlayState(bqPlayerPlay, SL_PLAYSTATE_PLAYING);
    FLUID_LOG(0, "SetPlayState %d", SL_RESULT_SUCCESS == result);
    (void)result;
}

